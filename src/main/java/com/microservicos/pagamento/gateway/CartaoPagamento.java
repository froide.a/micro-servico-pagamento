package com.microservicos.pagamento.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cartao")
public interface CartaoPagamento {

    @GetMapping("/cartao/{id}")
    Cartao getBypagamentoId(@PathVariable(name = "id") Long IdCartao);
}


