package com.microservicos.pagamento.gateway;

import javax.validation.constraints.NotNull;

public class Cartao {

    private long idCartao;

    public Cartao() {
    }

    public long getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(long idCartao) {
        this.idCartao = idCartao;
    }
}

