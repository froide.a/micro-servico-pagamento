package com.microservicos.pagamento.repository;

import com.microservicos.pagamento.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

    List<Pagamento> findByIdCartao(long IdCartao);
}
