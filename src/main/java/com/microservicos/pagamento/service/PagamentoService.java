package com.microservicos.pagamento.service;

import com.microservicos.pagamento.gateway.Cartao;
import com.microservicos.pagamento.gateway.CartaoPagamento;
import com.microservicos.pagamento.model.Pagamento;
import com.microservicos.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoPagamento cartaoPagamento;


    public Pagamento cadastrarPagamento(long IdCartao, Pagamento pagamento) {
        try {
                Cartao cartao = cartaoPagamento.getBypagamentoId(IdCartao);
                pagamento.setIdCartao(cartao.getIdCartao());
                return pagamentoRepository.save(pagamento);

        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o pagamento: " +
                    e.getCause().getCause().getMessage(), e);
        }
    }

        public List<Pagamento> consultarPagamentosPorId(long IdCartao) {
        try {
            List<Pagamento> listaPagamento = pagamentoRepository.findByIdCartao(IdCartao);
            return listaPagamento;
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao consultar pagamento: " +
                    e.getCause().getCause().getMessage(), e);
        }
    }
}
