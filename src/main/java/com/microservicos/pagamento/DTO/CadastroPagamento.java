package com.microservicos.pagamento.DTO;

import com.microservicos.pagamento.model.Pagamento;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CadastroPagamento {
    @NotNull(message = "Informe o Id do cartão.")
    private long idCartao;

    @NotEmpty(message = "Informe a descrição para o pagamento.")
    @Size(min = 3, max = 50, message = "A descrição deve conter entre 3 e 50 caracteres.")
    private String descricao;

    @NotNull(message = "Informe o valor do pagamento.")
    @DecimalMin(value = "10.00", message = "O pagamento mínimo é de R$ 10,00.")
    private BigDecimal valor;

    public CadastroPagamento() {
    }

    public long getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(long idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Pagamento converterParaPagamento(CadastroPagamento cadastroPagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(cadastroPagamentoRequest.getDescricao());
        pagamento.setValor(cadastroPagamentoRequest.getValor());
        return pagamento;
    }
}
