package com.microservicos.pagamento.DTO;

import com.microservicos.pagamento.model.Pagamento;

import java.math.BigDecimal;

public class PagamentoResponse {
    private long id;
    private long idCartao;
    private String descricao;
    private BigDecimal valor;

    public PagamentoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(long idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public PagamentoResponse converterParaCadastroPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setIdCartao(pagamento.getIdCartao());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }
}
