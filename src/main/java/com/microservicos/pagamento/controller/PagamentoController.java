package com.microservicos.pagamento.controller;

import com.microservicos.pagamento.DTO.CadastroPagamento;
import com.microservicos.pagamento.DTO.PagamentoResponse;
import com.microservicos.pagamento.gateway.CartaoPagamento;
import com.microservicos.pagamento.model.Pagamento;
import com.microservicos.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse pagamentoResponse (@RequestBody @Valid CadastroPagamento cadastroPagamento) {
        try {
            PagamentoResponse pagamentoResponse = new PagamentoResponse();
            Pagamento pagamento = pagamentoService.cadastrarPagamento(cadastroPagamento.getIdCartao(),
                    cadastroPagamento.converterParaPagamento(cadastroPagamento));
            return pagamentoResponse.converterParaCadastroPagamentoResponse(pagamento);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{cartaoId}")
    public List<PagamentoResponse> consultarPagamentosPorId(@PathVariable(name = "cartaoId") long cartaoId) {
        try {
            List<PagamentoResponse> listaPagamentoResponse = new ArrayList<>();
            List<Pagamento> listaPagamento = pagamentoService.consultarPagamentosPorId(cartaoId);

            for (Pagamento pagamento : listaPagamento) {
                PagamentoResponse pagamentoResponse = new PagamentoResponse();
                pagamentoResponse.setId(pagamento.getId());
                pagamentoResponse.setIdCartao(pagamento.getIdCartao());
                pagamentoResponse.setDescricao(pagamento.getDescricao());
                pagamentoResponse.setValor(pagamento.getValor());
                listaPagamentoResponse.add(pagamentoResponse);
            }
            return listaPagamentoResponse;

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}
